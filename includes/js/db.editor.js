$(function(){
    $(function() {
        $('#departamentsTableContainer').jtable({
            title: 'Кафедры',
            actions: {
                listAction: '/modules/departaments.php?action=list',
                createAction: '/modules/departaments.php?action=create',
                updateAction: '/modules/departaments.php?action=update',
                deleteAction: '/modules/departaments.php?action=delete'
            },
            fields: {
                id: {
                    key: true,
                    create: false,
                    edit: false,
                    list: false
                },
                name: {
                    title: 'Название кафедры'
                }
            }
        });
        $('#departamentsTableContainer').jtable('load');

        });
        $('#groupsTableContainer').jtable({
            title: 'Учебные группы',
            actions: {
                listAction: '/modules/groups.php?action=list',
                createAction: '/modules/groups.php?action=create',
                updateAction: '/modules/groups.php?action=update',
                deleteAction: '/modules/groups.php?action=delete'
            },
            fields: {
                id: {
                    key: true,
                    create: false,
                    edit: false,
                    list: false
                },
                name: {
                    title: 'Название группы'
                },
                direction: {
                    title: 'Направление'
                },
                profile: {
                    title: 'Профиль'
                },
                count: {
                    title: 'Кол-во человек'
                },
                creation_year: {
                    title: 'Год поступления'
                }
            }
        });
        $('#groupsTableContainer').jtable('load');

        $('#disciplinesTableContainer').jtable({
            title: 'Дисциплины',
            actions: {
                listAction: '/modules/disciplines.php?action=list',
                createAction: '/modules/disciplines.php?action=create',
                updateAction: '/modules/disciplines.php?action=update',
                deleteAction: '/modules/disciplines.php?action=delete'
            },
            fields: {
                id: {
                    key: true,
                    create: false,
                    edit: false,
                    list: false
                },
                name: {
                    title: 'Название дисциплины'
                },
                departament: {
                    title: 'Кафедра',
                    options: '/modules/disciplines.php?departament=getList'
                }
            }
        });
        $('#disciplinesTableContainer').jtable('load');

        $( "#teachersTable-edit-form" ).dialog({
          autoOpen: false,
          height: 400,
          width: 350,
          modal: true,
          buttons: {
            "Отмена": function() {
                $(this).dialog("close");
            },
            "Сохранить": function() {
                var msg = $('#jtable-edit-form').serialize();
                var key = $("#edit-id").val();
                var id = -1;
                if (key < 0){
                    $.ajax({
                        type: 'POST',
                        dataType: 'JSON',
                        url: '/modules/teachers.php?action=create',
                        data: msg,
                        success: function(data){
                            if (data.Result != "OK") alert("При сохранении произошла ошибка!");
                            var x = [];
                            $("#edit-disciplines option:selected").each(function(){
                                x.push($(this).attr('value'));
                            });
                            id = teachers.length;
                            teachers.push({
                                id: data.Record.id,
                                name: $("#edit-name").val(),
                                departament: $("#edit-departament").val(),
                                potentional_discipline:  x.join(',')
                            });
                            var row = '';
                            row += '<tr class="jtable-data-row jtable-row-even" data-record-key="'+teachers[id].id+'">';
                            row += '<td>'+teachers[id].name+'</td>';
                            row += '<td>'+GetValueName(departaments, teachers[id].departament)+'</td>';
                            row += '<td>'+GetValuesName(disciplines, teachers[id].potentional_discipline)+'</td>';
                            row += '<td class="jtable-command-column"><button title="Изменить" class="jtable-command-button jtable-edit-command-button"><span>Изменить</span></button></td>';
                            row += '<td class="jtable-command-column"><button title="Удалить" class="jtable-command-button jtable-delete-command-button"><span>Удалить</span></button></td>';
                            row += '</tr>';
                            table.append(row);

                            $("#teachersTable-edit-form").dialog("close");
                        },
                        error:  function(xhr, str){
                            alert('Возникла ошибка: ' + xhr.responseCode);
                        }
                    });
                }else{
                    $.ajax({
                        type: 'POST',
                        dataType: 'JSON',
                        url: '/modules/teachers.php?action=update',
                        data: msg,
                        success: function(data){
                            if (data.Result != "OK") alert("При сохранении произошла ошибка!");
                            for (var i = 0; i < teachers.length; i++) if (teachers[i].id == key) id = i;
                            var x = [];
                            $("#edit-disciplines option:selected").each(function(){
                                x.push($(this).attr('value'));
                            });
                            teachers[id].name = $("#edit-name").val();
                            teachers[id].departament = $("#edit-departament").val();
                            teachers[id].potentional_discipline = x.join(',');

                            var row = '';
                            row += '<td>'+teachers[id].name+'</td>';
                            row += '<td>'+GetValueName(departaments, teachers[id].departament)+'</td>';
                            row += '<td>'+GetValuesName(disciplines, teachers[id].potentional_discipline)+'</td>';
                            row += '<td class="jtable-command-column"><button title="Изменить" class="jtable-command-button jtable-edit-command-button"><span>Изменить</span></button></td>';
                            row += '<td class="jtable-command-column"><button title="Удалить" class="jtable-command-button jtable-delete-command-button"><span>Удалить</span></button></td>';
                            table.find('tr[data-record-key='+$("#edit-id").val()+']').html(row);
                            
                            $("#teachersTable-edit-form").dialog("close");
                        },
                        error:  function(xhr, str){
                            alert('Возникла ошибка: ' + xhr.responseCode);
                        }
                    });
                }
            }
          },
          close: function() {
            
          }
        });

        $( "#teachersTable-delete-form" ).dialog({
          autoOpen: false,
          modal: true,
          buttons: {
            "Отмена": function() {
                $(this).dialog("close");
            },
            "Удалить": function() {
                var msg = $('#jtable-edit-form').serialize();
                var key = $("#edit-id").val();
                var id = -1;
                $.ajax({
                    type: 'POST',
                    dataType: 'JSON',
                    url: '/modules/teachers.php?action=delete',
                    data: msg,
                    success: function(data){
                        if (data.Result != "OK") alert("При сохранении произошла ошибка!");
                        table.find('tr[data-record-key='+$("#edit-id").val()+']').remove();
                        $("#teachersTable-delete-form").dialog("close");
                    },
                    error:  function(xhr, str){
                        alert('Возникла ошибка: ' + xhr.responseCode);
                    }
                });
            }
          },
          close: function() {
            
          }
        });

        var table = $('#teachersTableContainer .jtable tbody');
        var teachers = [];
        var departaments = [];
        var disciplines = [];

        $(table).on('click', '.jtable-edit-command-button', function(){
            var key = $(this).parents('tr').data('record-key');
            var id = 0;
            for (var i = 0; i < teachers.length; i++) if (teachers[i].id == key) id = i;
            $("#edit-id").val(teachers[id].id);
            $("#edit-name").val(teachers[id].name);
            $("#edit-departament").val(teachers[id].departament);
            var x = teachers[id].potentional_discipline.split(',');
            $("#edit-disciplines option").removeAttr("selected");
            for (var i = 0; i < x.length; i++)
                $("#edit-disciplines option[value="+x[i]+"]").attr("selected", "selected");
            $("#teachersTable-edit-form").dialog("open");
        });
        $(table).on('click', '.jtable-delete-command-button', function(){
            var key = $(this).parents('tr').data('record-key');
            $("#edit-id").val(key);
            $("#teachersTable-delete-form").dialog("open");
        });

        $('#teachersTableContainer').on('click', '.jtable-toolbar-item-add-record', function(){
            $("#edit-id").val(-1);
            $("#edit-name").val('');
            $("#edit-departament").val(1);
            $("#edit-disciplines option").removeAttr("selected");
            $("#teachersTable-edit-form").dialog("open");
        });

        table.empty();
        $.getJSON('/modules/teachers.php?action=list', function(data){
            if (data.Result != "OK") return false;
            teachers = data.Records;
            $.getJSON('/modules/teachers.php?departament=getlist', function(values){
                if (values.Result != "OK") return false;
                departaments = values.Options;
                $('#edit-departament').empty();
                for (var i = 0; i < departaments.length; i++) $('#edit-departament').append('<option value="'+departaments[i].Value+'">'+departaments[i].DisplayText+'</option>');
                $.getJSON('/modules/teachers.php?disciplines=getlist', function(values){
                    if (values.Result != "OK") return false;
                    disciplines = values.Options;
                    $('#edit-disciplines').empty();
                    for (var i = 0; i < disciplines.length; i++) $('#edit-disciplines').append('<option value="'+disciplines[i].Value+'">'+disciplines[i].DisplayText+'</option>');
                    var row = '';
                    for (var i = 0; i < data.Records.length; i++){
                        row = '';
                        row += '<tr class="jtable-data-row jtable-row-even" data-record-key="'+data.Records[i].id+'">';
                        row += '<td>'+data.Records[i].name+'</td>';
                        row += '<td>'+GetValueName(departaments, data.Records[i].departament)+'</td>';
                        row += '<td>'+GetValuesName(disciplines, data.Records[i].potentional_discipline)+'</td>';
                        row += '<td class="jtable-command-column"><button title="Изменить" class="jtable-command-button jtable-edit-command-button"><span>Изменить</span></button></td>';
                        row += '<td class="jtable-command-column"><button title="Удалить" class="jtable-command-button jtable-delete-command-button"><span>Удалить</span></button></td>';
                        row += '</tr>';
                        table.append(row);
                    }
                });
            });
        });
});

function GetValueName(values, id){
    for (var i = 0; i < values.length; i++){
        if (values[i].Value == id) return values[i].DisplayText;
    }
}

function GetValuesName(values, ids){
    var x = ids.split(',');
    var res = [];
    for (var j = 0; j < x.length; j++){
        for (var i = 0; i < values.length; i++){
            if (values[i].Value == x[j]) res.push(values[i].DisplayText);
        }
    }
    return res.join(', ');
}