var changed = false;
var DISCIPLINES = [];
var table_index = 1;
var index = 1;
var DISCIPLINES_T = [];
var GROUPS = [];

function reindexTable() {
    table_index = 1;
    $('table > tbody > tr').each(function () {
        $(this).find('td:first').text(table_index);
        table_index++;
    });
}

$(function () {
    // инициализация модального окна "о приложении"
    $(document).on('click', '#show-modal-about', function () {
        $.ajax({
            url: '/pages/modal-about.html',
            success: function (data) {
                $('body').append(data);
                $('#modal-about').modal({
                    show: true
                });
            }
        });
        return false;
    });

    // инициализация модального окна "Поиск"
    $(document).on('click', '#show-modal-search', function () {
        $('#ajax-loader').show();
        $.ajax({
            url: '/pages/modal-search.html',
            success: function (data) {
                $('body').append(data);
                $('#modal-search').modal({
                    backdrop: false,
                    keyboard: true,
                    show: true
                });
                $('#search-list').attr('disabled', 'disabled');
                $('#search-result').attr('disabled', 'disabled');

                $('#ajax-loader').hide();
            }
        });
        return false;
    });

    // инициализация модального окна "Добавление дисциплины" --аудит.график
    $(document).on('click', '#show-modal-add-discipline-academic', function () {
        $('#ajax-loader').show();
        $.ajax({
            url: '/pages/modal-add-discipline-academic.html',
            success: function (data) {
                $('#modal-container').empty();
                $(data).appendTo('#modal-container');
                $('#modal-discipline').modal({
                    backdrop: false,
                    keyboard: true,
                    show: true
                });
                $('#ajax-loader').hide();
            }
        });
        return false;
    });

    // инициализация модального окна "Редактирование дисциплины"
    $(document).on('click', '#show-modal-edit-discipline-academic', function () {
        var id = $(this).data('id');
        var thisRow = $(this).parent().parent();

        $('#ajax-loader').show();
        $.ajax({
            url: '/pages/modal-add-discipline-academic.html',
            success: function (data) {
                $('#modal-container').empty();
                $(data).appendTo('#modal-container');
                $('#add_change_index').val(id);

                $('#DISCIPLINE_NAME').val(DISCIPLINES[id].DISCIPLINE_ID);
                $('#FORM_CONTROL').val(DISCIPLINES[id].FORM_CONTROL_ID);
                $('#LECTURES').val(DISCIPLINES[id].LECTURES);
                $('#PRACTICAL_WORK').val(DISCIPLINES[id].PRACTICAL_WORK);
                $('#LABORATORY_WORK').val(DISCIPLINES[id].LABORATORY_WORK);
                $('#LECTURES_TOTAL_HOURS').val(DISCIPLINES[id].LECTURES_TOTAL_HOURS);
                $('#PRACTICAL_WORK_TOTAL_HOURS').val(DISCIPLINES[id].PRACTICAL_WORK_TOTAL_HOURS);
                $('#LABORATORY_WORK_TOTAL_HOURS').val(DISCIPLINES[id].LABORATORY_WORK_TOTAL_HOURS);

                $('#modal-discipline-label').html('Изменить дисциплину');
                $('#modal-discipline').modal({
                    backdrop: false,
                    keyboard: true,
                    show: true
                });
                $('#ajax-loader').hide();
            }
        });
        return false;
    });

    // загрука данных о группе из БД (после выбора группы) --аудит.график
    $('#group').on('change', function () {
        if ($(this).val() != -1) {
            var GROUP_ID = $(this).val();
            $('#ajax-loader').show();
            $.ajax({
                url: '/modules/report_academic.php?GROUP_ID=' + GROUP_ID,
                dataType: 'json',
                success: function (data) {
                    $('#direction').val(data.direction);
                    $('#profile').val(data.profile);

                    date = new Date();
                    var currYear = date.getFullYear(),
                        currMonth = date.getMonth(),
                        currCourse,
                        currSemester,
                        creationYear = data.creation_year;

                    if (currMonth >= 8) {
                        currCourse = (currYear - creationYear) + 1;
                    } else {
                        currCourse = currYear - creationYear;
                    }
                    currSemester = 2 * currCourse - 1;
                    $('#course').val(currCourse);
                    $('#semester').val(currSemester);
                    $('#ajax-loader').hide();
                }
            });
        }
    });

    // функция добавления/изменения дисциплины, привязана к кнопке "применить" в модальном окне
    $(document).on('click', '#apply_discipline_modal', function () {
        var ID = $('#add_change_index').val();

        if (($('#DISCIPLINE_NAME option:selected').val() == -1) ||
            ($('#LECTURES').val() == '') ||
            ($('#PRACTICAL_WORK').val() == '') ||
            ($('#LABORATORY_WORK').val() == '') ||
            ($('#LECTURES_TOTAL_HOURS').val() == '') ||
            ($('#PRACTICAL_WORK_TOTAL_HOURS').val() == '') ||
            ($('#LABORATORY_WORK_TOTAL_HOURS').val() == '') ||
            ($('#FORM_CONTROL').val() == '')) {
            alert('Ошибка! Есть незаполненные пункты!');
            return;
        }

        var element = document.getElementById("FORM_CONTROL"),
            selectedOptions = element.selectedOptions,
            form_control = [],
            form_control_id = $("#FORM_CONTROL").val() || [];

        for (var i = 0; i < selectedOptions.length; i++) {
            form_control.push(selectedOptions[i].text);
            form_control_id.push(selectedOptions[i].value);
        }

        var st = false;
        // если в модальном окне hidden(add_change_index) имеет значение -1 то происходит добавление, иначе - изменение
        if (ID >= 0) {
            for (var i = 0; i < DISCIPLINES.length; i++) if (DISCIPLINES[i].ID == ID) cid = i;
            st = true;
            var number = $('#'+ID+' td._number').text();
            DISCIPLINES[cid].DISCIPLINE_NAME = $('#DISCIPLINE_NAME option:selected').text();
            DISCIPLINES[cid].DISCIPLINE_ID = $('#DISCIPLINE_NAME option:selected').val();
            DISCIPLINES[cid].FORM_CONTROL = form_control.join();
            DISCIPLINES[cid].FORM_CONTROL_ID = form_control_id;
            DISCIPLINES[cid].LECTURES = $('#LECTURES').val();
            DISCIPLINES[cid].PRACTICAL_WORK = $('#PRACTICAL_WORK').val();
            DISCIPLINES[cid].LABORATORY_WORK = $('#LABORATORY_WORK').val();
            DISCIPLINES[cid].LECTURES_TOTAL_HOURS = $('#LECTURES_TOTAL_HOURS').val();
            DISCIPLINES[cid].PRACTICAL_WORK_TOTAL_HOURS = $('#PRACTICAL_WORK_TOTAL_HOURS').val();
            DISCIPLINES[cid].LABORATORY_WORK_TOTAL_HOURS = $('#LABORATORY_WORK_TOTAL_HOURS').val();
        } else {
            var number = table_index;
            table_index++;
            ID = index;
            index++;
            cid = DISCIPLINES.length;
            DISCIPLINES.push({
                ID: ID,
                DISCIPLINE_NAME: $('#DISCIPLINE_NAME option:selected').text(),
                DISCIPLINE_ID: $('#DISCIPLINE_NAME option:selected').val(),
                FORM_CONTROL: form_control.join(),
                FORM_CONTROL_ID: form_control_id,
                LECTURES: $('#LECTURES').val(),
                PRACTICAL_WORK: $('#PRACTICAL_WORK').val(),
                LABORATORY_WORK: $('#LABORATORY_WORK').val(),
                LECTURES_TOTAL_HOURS: $('#LECTURES_TOTAL_HOURS').val(),
                PRACTICAL_WORK_TOTAL_HOURS: $('#PRACTICAL_WORK_TOTAL_HOURS').val(),
                LABORATORY_WORK_TOTAL_HOURS: $('#LABORATORY_WORK_TOTAL_HOURS').val()
            });
        }

        var row = '';
        row = '<td class="_number">' + number + '</td>';
        row += '<td>' + DISCIPLINES[cid].DISCIPLINE_NAME + '</td>';
        row += '<td>' + DISCIPLINES[cid].LECTURES + '</td>';
        row += '<td>' + DISCIPLINES[cid].PRACTICAL_WORK + '</td>';
        row += '<td>' + DISCIPLINES[cid].LABORATORY_WORK + '</td>';
        row += '<td>' + DISCIPLINES[cid].LECTURES_TOTAL_HOURS + '</td>';
        row += '<td>' + DISCIPLINES[cid].PRACTICAL_WORK_TOTAL_HOURS + '</td>';
        row += '<td>' + DISCIPLINES[cid].LABORATORY_WORK_TOTAL_HOURS + '</td>';
        row += '<td>' + DISCIPLINES[cid].FORM_CONTROL + '</td>';
        row += '<td class="actions"><button class="btn btn-default btn-sm" id="show-modal-edit-discipline-academic" ' +
            'data-id="' + ID + '"><span class="glyphicon glyphicon-pencil"></span></button>' +
            '<button class="btn btn-default btn-sm" onclick="$(this).parent().parent().remove();' +
            'DISCIPLINES.splice(' + cid + ', 1); reindexTable();">' +
            '<span class="glyphicon glyphicon-remove"></span></button></td>';

        if (st) $('#' + ID).html(row); else $('table > tbody').append('<tr id="' + ID + '">' + row + '</tr>');
        $('#close_discipline_modal').click();
    });

    // функция генерации отчета --аудит.график
    $(document).on('click', '#save_academic_report', function () {

        report_date = new Date();
        var currYear = report_date.getFullYear(),
            nextYear = currYear + 1;

        var GENERAL = [];
        GENERAL.push({
            COURSE_AND_SEMESTER: $('#course').val() + ' курса ' + $('#semester').val() + ' семестра',
            GROUP_AND_YEAR: $('#group option:selected').text() + ' на ' + $('#half_year option:selected').val()
                + ' полугодие ' + currYear + '/' + nextYear + ' учебного года',
            DIRECTION_AND_PROFILE: 'Направление: ' + $('#direction').val() + ', профиль: ' + $('#profile').val(),
            HOURS_PER_WEEK: $('#hours_per_week').val()
        });

        var DISCIPLINES_ID = [];
        for (var i = 0; i < DISCIPLINES.length; i++) {
            DISCIPLINES_ID.push('<' + DISCIPLINES[i].DISCIPLINE_ID + '>');
        }
        if (DISCIPLINES.length == 0) {
            if (($('#group option:selected').val() == -1) ||
                ($('#direction').val() == '') ||
                ($('#profile').val() == '') ||
                ($('#course').val() == '') ||
                ($('#semester').val() == '') ||
                ($('#hours_per_week').val() == '')) {
                alert('Ошибка! Есть незаполненные пункты!');
            }
            else {
                alert('Ошибка! Не добавлено ни одного предмета!');
            }
        }
        else {
            $('#ajax-loader').show();
            $.ajax({
                type: 'POST',
                url: '/modules/report_academic.php',
                data: {
                    CREATE_ORDER: 1,
                    GENERAL_INFO: GENERAL[0],
                    DISCIPLINES: DISCIPLINES,
                    DISCIPLINES_ID: DISCIPLINES_ID,
                    HALF_YEAR: $('#half_year option:selected').val(),
                    GROUP_NAME: $('#group option:selected').text(),
                    GROUP_ID: $('#group option:selected').val(),
                    GROUP_COURSE: $('#course').val()
                },
                success: function (data) {
                    console.log(data);
                    $('#download-link').attr('href', data);
                    $('#modalSuccess').modal({
                        width: 300,
                        show: true,
                        backdrop: false
                    });
                    $('#ajax-loader').hide();
                }
            });
            return false;
        }
    });

    // инициализация модального окна "Добавление дисциплины" --педагог.график
    $(document).on('click', '#show-modal-add-discipline-teachers', function () {
        $('#ajax-loader').show();
        $.ajax({
            url: '/pages/modal-add-discipline-teachers.html',
            success: function (data) {
                GROUPS = [];
                $('#modal-container').empty();
                $(data).appendTo('#modal-container');
                $('#modal-discipline-teachers').modal({
                    backdrop: false,
                    keyboard: true,
                    show: true
                });
                $('#ajax-loader').hide();
            }
        });
        return false;
    });

    // загрука данных о группе из БД (после выбора группы) --педагог.график
    $(document).on('change', '#group_teachers', function () {
        if ($(this).val() != -1) {
            var GROUP_ID = $(this).val();
            $('#ajax-loader').show();
            $.ajax({
                url: '/modules/report_teachers.php',
                type: 'POST',
                data: {
                    GROUP_ID: GROUP_ID
                },
                dataType: 'json',
                success: function (data) {
                    $('#count-teachers').val(data.count);
                    date = new Date();
                    var currYear = date.getFullYear(),
                        currMonth = date.getMonth(),
                        currCourse,
                        creationYear = data.creation_year;

                    if (currMonth >= 8) {
                        currCourse = (currYear - creationYear) + 1;
                    } else {
                        currCourse = currYear - creationYear;
                    }
                    $('#course-teachers').val(currCourse);
                    $('#ajax-loader').hide();
                }
            });
        }
    });

    // добавление информ. о группах в список
    $(document).on('click', '#add-to-list', function () {
        var groupInfo = 'Группа: ' + $('#group_teachers option:selected').text() + ' (' + $('#course-teachers').val()
            + ' курс, ' + $('#count-teachers').val() + ' человек(а), ' + $('#halfyear-teachers option:selected').val()
            + ' полугодие, Л: ' + $('#lectures').val() + ', ПР.Р.: ' + $('#practical').val() + ', Л.Р.: ' + $('#laboratory').val() + ')';

        var id = index;
        index++;
        GROUPS.push({
            ID: id,
            GROUP_ID: $('#group_teachers option:selected').val(),
            GROUP_NAME: $('#group_teachers option:selected').text(),
            COURSE: $('#course-teachers').val(),
            COUNT: $('#count-teachers').val(),
            HALFYEAR: $('#halfyear-teachers option:selected').val(),
            LECTURES: $('#lectures').val(),
            PRACTICAL: $('#practical').val(),
            LABORATORY: $('#laboratory').val()
        });

        $('#discipline-list').append($('<option value="'+id+'">' + groupInfo + '</option>'));

        $('#group_teachers').val(-1);
        $('#course-teachers').val('');
        $('#count-teachers').val('');
        $('#halfyear-teachers').val(1);
        $('#lectures').val('');
        $('#practical').val('');
        $('#laboratory').val('');
    });

    // изменение информ. о группах в список
    $(document).on('click', '#change-to-list', function () {
        var groupInfo = 'Группа: ' + $('#group_teachers option:selected').text() + ' (' + $('#course-teachers').val()
            + ' курс, ' + $('#count-teachers').val() + ' человек(а), ' + $('#halfyear-teachers option:selected').val()
            + ' полугодие, Л: ' + $('#lectures').val() + ', ПР.Р.: ' + $('#practical').val() + ', Л.Р.: ' + $('#laboratory').val() + ')';

        var id = $(this).data('id');
        for (var i = 0; i < GROUPS.length; i++)
            if (GROUPS[i].ID == id){
                GROUPS[i].GROUP_ID = $('#group_teachers option:selected').val(),
                GROUPS[i].GROUP_NAME = $('#group_teachers option:selected').text(),
                GROUPS[i].COURSE = $('#course-teachers').val(),
                GROUPS[i].COUNT = $('#count-teachers').val(),
                GROUPS[i].HALFYEAR = $('#halfyear-teachers option:selected').val(),
                GROUPS[i].LECTURES = $('#lectures').val(),
                GROUPS[i].PRACTICAL = $('#practical').val(),
                GROUPS[i].LABORATORY = $('#laboratory').val()
            }

        $('#discipline-list option[value='+id+']').text(groupInfo);

        $('#group_teachers').val(-1);
        $('#course-teachers').val('');
        $('#count-teachers').val('');
        $('#halfyear-teachers').val(1);
        $('#lectures').val('');
        $('#practical').val('');
        $('#laboratory').val('');

        $('#add-to-list').show();
        $('#change-to-list').hide();
        $('#cancel-change').hide();
    });

    // удаление элементов из списка информ. о группах
    $(document).on('click', '#delete-element', function () {
        var id = $('#discipline-list :selected').val();
        $('#discipline-list :selected').remove();

        for (var i = 0; i < GROUPS.length; i++){
            if (GROUPS[i].ID == id) GROUPS.splice(i, 1);
        }
    });

    // изменение элементов из списка информ. о группах
    $(document).on('click', '#change-element', function () {
        var id = $('#discipline-list :selected').val();

        for (var i = 0; i < GROUPS.length; i++)
            if (GROUPS[i].ID == id){
                $('#group_teachers').val(GROUPS[i].GROUP_ID);
                $('#course-teachers').val(GROUPS[i].COURSE);
                $('#count-teachers').val(GROUPS[i].COUNT);
                $('#halfyear-teachers').val(GROUPS[i].HALFYEAR);
                $('#lectures').val(GROUPS[i].LECTURES);
                $('#practical').val(GROUPS[i].PRACTICAL);
                $('#laboratory').val(GROUPS[i].LABORATORY);

                $('#add-to-list').hide();
                $('#change-to-list').data('id', id).show();
                $('#cancel-change').show();
            }
    });

    // отмена изменений
    $(document).on('click', '#cancel-change', function () {
        $('#group_teachers').val(-1);
        $('#course-teachers').val('');
        $('#count-teachers').val('');
        $('#halfyear-teachers').val(1);
        $('#lectures').val('');
        $('#practical').val('');
        $('#laboratory').val('');

        $('#add-to-list').show();
        $('#change-to-list').hide();
        $('#cancel-change').hide();
    });

    // инициализация модального окна "Редактирование дисциплины"
    $(document).on('click', '#show-modal-edit-discipline-teachers', function () {
        var id = $(this).data('id');

        $('#ajax-loader').show();
        $.ajax({
            url: '/pages/modal-add-discipline-teachers.html',
            success: function (data) {
                $('#modal-container').empty();
                $(data).appendTo('#modal-container');
                
                $('#add_change_index').val(id);

                $('#DISCIPLINE_NAME').val(id).attr('disabled','disabled');
                for (var i = 0; i < DISCIPLINES_T.length; i++) if (DISCIPLINES_T[i].ID == id) did = i;
                for (var i = 0; i < DISCIPLINES_T[did].GROUPS.length; i++){
                    var groupInfo = 'Группа: ' + DISCIPLINES_T[did].GROUPS[i].GROUP_NAME + ' (' + DISCIPLINES_T[did].GROUPS[i].COURSE
            + ' курс, ' + DISCIPLINES_T[did].GROUPS[i].COUNT + ' человек(а), ' + DISCIPLINES_T[did].GROUPS[i].HALFYEAR
            + ' полугодие, Л: ' + DISCIPLINES_T[did].GROUPS[i].LECTURES + ', ПР.Р.: ' + DISCIPLINES_T[did].GROUPS[i].PRACTICAL + ', Л.Р.: ' + DISCIPLINES_T[did].GROUPS[i].LABORATORY + ')';
                    $('#discipline-list').append($('<option value="'+DISCIPLINES_T[did].GROUPS[i].ID+'">' + groupInfo + '</option>'));
                }
                GROUPS = DISCIPLINES_T[did].GROUPS;

                $('#modal-discipline-label').html('Изменить дисциплину');
                $('#modal-discipline-teachers').modal({
                    backdrop: false,
                    keyboard: true,
                    show: true
                });
                $('#ajax-loader').hide();
            }
        });
        return false;
    });

        // функция добавления/изменения дисциплины, привязана к кнопке "применить" в модальном окне
    $(document).on('click', '#apply_teachers_modal', function () {
        var ID = $('#add_change_index').val();
        // если в модальном окне hidden(add_change_index) имеет значение -1 то происходит добавление, иначе - изменение
        var discipline_id = $('#DISCIPLINE_NAME option:selected').val();
        if ((discipline_id < 0)||(GROUPS.length < 1)){
            alert('Не заполнены обязательные поля!');
            return;
        }

        var id = -1;
        var curnumber = 1;
        var row = '';
        var st = false;

        if (ID >= 0) {
            for (var i = 0; i < DISCIPLINES_T.length; i++) if (DISCIPLINES_T[i].ID == discipline_id) id = i;
            DISCIPLINES_T[id].GROUPS = GROUPS;
            curnumber = $('#'+discipline_id).find('td._number').text();
            $('tr[data-id='+discipline_id+']').remove();
            st = true;
        } else {
            for (var i = 0; i < DISCIPLINES_T.length; i++) if (DISCIPLINES_T[i].ID == discipline_id) id = i;
            if (id < 0){
                id = DISCIPLINES_T.length;
                DISCIPLINES_T.push({
                    ID: discipline_id,
                    DISCIPLINE_NAME: $('#DISCIPLINE_NAME option:selected').text(),
                    GROUPS: GROUPS
                });
                curnumber = table_index;
                table_index++;
            }else{
                DISCIPLINES_T[id].GROUPS = DISCIPLINES_T[id].GROUPS.concat(GROUPS);
                curnumber = $('#'+discipline_id).find('td._number').text();
                $('tr[data-id='+discipline_id+']').remove();
                st = true;
            }
        }

        for (var i = 0; i < DISCIPLINES_T[id].GROUPS.length; i++){
            row = '';
            if (i == 0){
                row += '<td class="_number" rowspan="'+DISCIPLINES_T[id].GROUPS.length+'">' + curnumber + '</td>';
                row += '<td class="_name" rowspan="'+DISCIPLINES_T[id].GROUPS.length+'">' + DISCIPLINES_T[id].DISCIPLINE_NAME + '</td>';
            }
            row += '<td>' + DISCIPLINES_T[id].GROUPS[i].GROUP_NAME + '</td>';
            row += '<td>' + DISCIPLINES_T[id].GROUPS[i].COURSE + '</td>';
            row += '<td>' + DISCIPLINES_T[id].GROUPS[i].COUNT + '</td>';
            row += '<td>' + DISCIPLINES_T[id].GROUPS[i].HALFYEAR + '</td>';
            row += '<td>' + DISCIPLINES_T[id].GROUPS[i].LECTURES + '</td>';
            row += '<td>' + DISCIPLINES_T[id].GROUPS[i].PRACTICAL + '</td>';
            row += '<td>' + DISCIPLINES_T[id].GROUPS[i].LABORATORY + '</td>';
            if (i == 0){
                row += '<td class="_buttons" rowspan="'+DISCIPLINES_T[id].GROUPS.length+'"><button class="btn btn-default btn-sm" id="show-modal-edit-discipline-teachers" ' +
            'data-id="' + discipline_id + '"><span class="glyphicon glyphicon-pencil"></span></button>' +
            '<button class="btn btn-default btn-sm" onclick="$(\'#'+discipline_id+'\').remove(); $(\'tr[data-id='+discipline_id+']\').remove();' +
            'DISCIPLINES_T.splice('+id+', 1); reindexTable();">' +
            '<span class="glyphicon glyphicon-remove"></span></button></td>';
            }
            if (i == 0)
                if (st) $('#'+discipline_id).html(row); else $('table > tbody').append('<tr id="'+discipline_id+'">'+row+'</tr>'); 
            else
                if (i == 1) 
                    $('#'+discipline_id).after('<tr data-id="'+discipline_id+'">'+row+'</tr>');
                else
                    $('tr[data-id='+discipline_id+']:last').after('<tr data-id="'+discipline_id+'">'+row+'</tr>');
        }
        $('#close_teachers_modal').click();
    });

    /*----- Модуль поиска -----*/

    $(document).on('change', '#search-criterion', function () {
        $('#search-list').removeAttr('disabled');
        if ($(this).val() == 0) {
            $('#search-list').empty();
            $("#search-list").append($('<option value="-1">не выбрано</option>'));
            $.ajax({
                url: '/modules/search.php',
                type: 'POST',
                data: {
                  SEARCH_TYPE: 'teachers'
                },
                dataType: 'json',
                success: function (data) {
                    for (var i = 0; i < data.length; i++) {
                        $("#search-list").append($('<option value="' + data[i].potentional_discipline
                            + '">' + data[i].name + '</option>') );
                    }
                }
            });
        }
        if ($(this).val() == 1) {
            $('#search-list').empty();
            $("#search-list").append($('<option value="-1">не выбрано</option>'));
            $.ajax({
                url: '/modules/search.php',
                type: 'POST',
                data: {
                    SEARCH_TYPE: 'disciplines'
                },
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    for (var i = 0; i < data.length; i++) {
                        $("#search-list").append($('<option value="' + data[i].id
                            + '">' + data[i].name + '</option>') );
                    }
                }
            });
        }
    });

    $(document).on('change', '#search-list', function () {
        $('#search-result').removeAttr('disabled');
        if ($(this).val() != -1) {
            if($('#search-criterion').val() == 0) {
                $('#search-result').empty();
                $.ajax({
                    url: '/modules/search.php',
                    type: 'POST',
                    data: {
                        SEARCH_RESULT_TYPE: 'teachers',
                        SEARCH_RESULT: $('#search-list').val()
                    },
                    dataType: 'json',
                    success: function (data) {
                        console.log(data);
                        for (var i = 0; i < data.length; i++) {
                            $("#search-result").append($('<option>' + data[i].name + '</option>') );
                        }
                    }
                });
            }
            if($('#search-criterion').val() == 1) {
                $('#search-result').empty();
                $.ajax({
                    url: '/modules/search.php',
                    type: 'POST',
                    data: {
                        SEARCH_RESULT_TYPE: 'disciplines',
                        SEARCH_RESULT: $('#search-list').val()
                    },
                    dataType: 'json',
                    success: function (data) {
                        console.log(data);
                        for (var i = 0; i < data.length; i++) {
                            $("#search-result").append($('<option>' + data[i].name + '</option>') );
                        }
                    }
                });
            }
        }
    });

    $(document).on('click', '#save_teachers_report', function () {
        $('#ajax-loader').show();
        $.ajax({
            type: 'POST',
            url: '/modules/report_teachers.php',
            data: {
                CREATE_ORDER: 1,
                DEPARTMENT: '',
                DISCIPLINES: DISCIPLINES_T
            },
            success: function (data) {
                $('#download-link').attr('href', data);
                $('#modalSuccess').modal({
                    width: 300,
                    show: true,
                    backdrop: false
                });
                $('#ajax-loader').hide();
                console.log(data);
            }
        });
    });

    return false;

 });