<?php
/**
 * Веб-приложение PHP2Excel
 * Автор: Сергей Минин
 * Дата создания: 06.04.2014
 * Версия: 0.0.1
 */

require_once 'config/dbconn.php';

require_once 'modules/routing.php';

include 'pages/main.html';