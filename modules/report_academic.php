<?php

include_once $_SERVER['DOCUMENT_ROOT'] . '/config/dbconn.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/phpexcel/PHPExcel.php';
function createAcademicReport($file, $hashTags = array(), $disciplines = array(), $snippet)
{
    $objPHPExcel = PHPExcel_IOFactory::load($file);
    $objPHPExcel->setActiveSheetIndex(0);
    $activeSheet = $objPHPExcel->getActiveSheet();
    foreach ($activeSheet->getRowIterator() as $currRow) {
        $cellIterator = $currRow->getCellIterator();
        foreach ($cellIterator as $currCell) {
            $cellValue = $currCell->getValue();
            if (preg_match("/^#([^%]+)#$/sei", $cellValue, $match)) {
                $currColumn = $currCell->getColumn();
                $currRow = $currCell->getRow();

                $currHashTag = $match['1'];

                if (isset ($hashTags[$currHashTag]) AND is_string($hashTags[$currHashTag])) {
                    $currCell->setValue($hashTags[$currHashTag]);
                } else if (isset ($hashTags[$currHashTag]) AND is_array($hashTags[$currHashTag])) {
                    $crew_template = $activeSheet->getStyle($currColumn . $currRow);
                    foreach ($hashTags[$currHashTag] as $_colVal) {
                        $activeSheet->duplicateStyle($crew_template, $currColumn . $currRow);
                        $objPHPExcel->getActiveSheet()->SetCellValue($currColumn . $currRow, $_colVal);
                        $currRow = $currRow + 1;
                    }

                } else if (isset ($hashTags[$currHashTag]) AND is_numeric($hashTags[$currHashTag])) {
                    $currCell->setValue($hashTags[$currHashTag]);
                } else if (isset ($hashTags[$currHashTag])) {
                    $objPHPExcel->getActiveSheet()->getStyle('C' . $i)->getNumberFormat()
                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    $currCell->setValue($hashTags[$currHashTag]);
                }
            }
        }
    }
    $anchor = 16;
    foreach ($disciplines as $id => $discipline) {
        $activeSheet->setCellValue('A' . $anchor, ($id + 1));
        $activeSheet->mergeCells('A' . $anchor . ':A' . ($anchor + 2));

        $activeSheet->setCellValue('B' . $anchor, $discipline['DISCIPLINE_NAME']);
        $activeSheet->mergeCells('B' . $anchor . ':B' . ($anchor + 2));

        $activeSheet->setCellValue('C' . $anchor, 'Лекции');
        $activeSheet->setCellValue('C' . ($anchor + 1), 'Упражн.');
        $activeSheet->setCellValue('C' . ($anchor + 2), 'Лаборат.');

        $activeSheet->setCellValue('D' . $anchor, $discipline['LECTURES']);
        $activeSheet->setCellValue('D' . ($anchor + 1), $discipline['PRACTICAL_WORK']);
        $activeSheet->setCellValue('D' . ($anchor + 2), $discipline['LABORATORY_WORK']);

        $activeSheet->setCellValue('E' . $anchor, $discipline['LECTURES_TOTAL_HOURS']);
        $activeSheet->setCellValue('E' . ($anchor + 1), $discipline['PRACTICAL_WORK_TOTAL_HOURS']);
        $activeSheet->setCellValue('E' . ($anchor + 2), $discipline['LABORATORY_WORK_TOTAL_HOURS']);
        $activeSheet->setCellValue('X' . $anchor, $discipline['FORM_CONTROL']);

        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array(
                        'rgb' => '000000'
                    ),
                ),
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_STYLE_ALIGNMENT::VERTICAL_CENTER,
            )
        );
        $activeSheet->getStyle('A' . $anchor . ':Y' . ($anchor + 2))->applyFromArray($styleArray);
        $anchor = $anchor + 3;
    }

    $objPHPExcel->getActiveSheet()->setTitle('Новый лист');

    $objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
    $objWriter->save($_SERVER['DOCUMENT_ROOT'] . '/reports/' . $snippet . '.xls');
    $objPHPExcel->disconnectWorksheets();
    unset($objPHPExcel);
}

$getGroup = mysql_query("SELECT id, name FROM groups")
or die(mysql_error());
$groups = array();
while ($item = mysql_fetch_assoc($getGroup)) {
    $groups[] = $item;
}

$getDiscipline = mysql_query("SELECT * FROM disciplines")
or die(mysql_error());
$disciplines = array();
while ($item = mysql_fetch_assoc($getDiscipline)) {
    $disciplines[] = $item;
}
if (isset($_GET['GROUP_ID'])) {
    $groupInfo = array();
    $getCurrGroupInfo = mysql_query("SELECT * FROM groups WHERE id = '" . $_GET['GROUP_ID'] . "'")
    or die(mysql_error());
    $groupInfo = mysql_fetch_assoc($getCurrGroupInfo);
    echo json_encode($groupInfo);
}
if (isset($_POST['CREATE_ORDER'])) {
    $filename = 'academic_report_' . date('Y_m_d_H_i_s');
    createAcademicReport(
        $_SERVER['DOCUMENT_ROOT'] . '/excel_template/academic.xls',
        $_POST['GENERAL_INFO'],
        $_POST['DISCIPLINES'],
        $filename
    );
    echo '/reports/' . $filename . '.xls';
    $sql =  "
        INSERT INTO
            reports_academic (`group`, `year`, `half_year`, `course`, `disciplines`, `results`)
        VALUES ("
        . $_POST['GROUP_ID'] . ","
        . date('Y') . ","
        . $_POST['HALF_YEAR'] . ","
        . $_POST['GROUP_COURSE'] . ", '"
        . implode(",", $_POST['DISCIPLINES_ID']). "','" . serialize($_POST['DISCIPLINES']) . "')
    ";
    mysql_query($sql) or die (mysql_error() . $sql);


    // TODO(developer): сделать запись данных в БД


}
