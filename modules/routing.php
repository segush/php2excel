<?php
$url = preg_replace('/[^a-zа-я0-9:\/?=&\._\-%]/iu', '', urldecode($_SERVER['REQUEST_URI']));
$url = preg_replace('#^/#', '', $url);
$url = preg_replace('/\?.*/', '', $url);
$items = explode('/', $url);
$count = count($items);
$items[$count-1] = preg_replace('/(\.htm|\.html|\.php)$/i', '', $items[$count-1]);
if ($items[$count-1] === '') {
    unset($items[$count - 1]);
}
$main_url = $items;
if ((isset($main_url[0])) and ($main_url[0] !== '')) {
    $main_url['page'] = $main_url[0];
} else {
    $main_url['page'] = "index";
}
$main_url['get'] = $_GET;

if ($main_url['page'] == 'index') {
    $WORK_AREA = $_SERVER['DOCUMENT_ROOT'] . '/pages/reports-all.html';
} else {
    if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/pages/' . $main_url['page'] . '.html')) {
        $WORK_AREA = $_SERVER['DOCUMENT_ROOT'] . '/pages/' . $main_url['page'] . '.html';
    } else {
        $WORK_AREA = $_SERVER['DOCUMENT_ROOT'] . '/pages/404.html';
    }
}
unset($items);