<?php

include_once $_SERVER['DOCUMENT_ROOT'] . '/config/dbconn.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/phpexcel/PHPExcel.php';

function createTeacherReport($file, $hashTags = array(), $disciplines = array(), $snippet)
{
    $objPHPExcel = PHPExcel_IOFactory::load($file);
    $objPHPExcel->setActiveSheetIndex(0);
    $activeSheet = $objPHPExcel->getActiveSheet();
    foreach ($activeSheet->getRowIterator() as $currRow) {
        $cellIterator = $currRow->getCellIterator();
        foreach ($cellIterator as $currCell) {
            $cellValue = $currCell->getValue();
            if (preg_match("/^#([^%]+)#$/sei", $cellValue, $match)) {
                $currColumn = $currCell->getColumn();
                $currRow = $currCell->getRow();

                $currHashTag = $match['1'];

                if (isset ($hashTags[$currHashTag]) AND is_string($hashTags[$currHashTag])) {
                    $currCell->setValue($hashTags[$currHashTag]);
                } else if (isset ($hashTags[$currHashTag]) AND is_array($hashTags[$currHashTag])) {
                    $crew_template = $activeSheet->getStyle($currColumn . $currRow);
                    foreach ($hashTags[$currHashTag] as $_colVal) {
                        $activeSheet->duplicateStyle($crew_template, $currColumn . $currRow);
                        $objPHPExcel->getActiveSheet()->SetCellValue($currColumn . $currRow, $_colVal);
                        $currRow = $currRow + 1;
                    }

                } else if (isset ($hashTags[$currHashTag]) AND is_numeric($hashTags[$currHashTag])) {
                    $currCell->setValue($hashTags[$currHashTag]);
                } else if (isset ($hashTags[$currHashTag])) {
                    $objPHPExcel->getActiveSheet()->getStyle('C' . $i)->getNumberFormat()
                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                    $currCell->setValue($hashTags[$currHashTag]);
                }
            }
        }
    }
    $anchor = 6;
    foreach ($disciplines as $discipline) {
        // запись названия дисциплины
        $activeSheet->setCellValue('A' . $anchor, $discipline['DISCIPLINE_NAME']);
        $activeSheet->mergeCells('A' . $anchor . ':T' . $anchor);

        $activeSheet->getStyle('A' . $anchor . ':T' . $anchor)->applyFromArray(
            array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'FFFF99'),
                )
            )
        );
        $activeSheet->getStyle('A' . $anchor . ':T' . $anchor)->getFont()->setBold(true);

        $anchor++;

        foreach ($discipline['GROUPS'] as $groups) {
            $activeSheet->setCellValue('A' . $anchor, $groups['COURSE']);
            $activeSheet->setCellValue('B' . $anchor, $groups['GROUP_NAME']);
            $activeSheet->setCellValue('C' . $anchor, $groups['COUNT']);

            if ($groups['HALFYEAR'] == 1) {
                $activeSheet->setCellValue('D' . $anchor, $groups['LECTURES']);
                $activeSheet->setCellValue('E' . $anchor, $groups['PRACTICAL']);
                $activeSheet->setCellValue('F' . $anchor, $groups['LABORATORY']);

                $activeSheet->setCellValue('I' . $anchor, '=(C' . $anchor . ' * 0.35)');
                $activeSheet->setCellValue('P' . $anchor, '=D' . $anchor);
                $activeSheet->setCellValue('Q' . $anchor, '=SUM(E' . $anchor . ':I' . $anchor . ')');
            } else {
                $activeSheet->setCellValue('J' . $anchor, $groups['LECTURES']);
                $activeSheet->setCellValue('K' . $anchor, $groups['PRACTICAL']);
                $activeSheet->setCellValue('L' . $anchor, $groups['LABORATORY']);

                $activeSheet->setCellValue('O' . $anchor, '=(C' . $anchor . ' * 0.35)');
                $activeSheet->setCellValue('P' . $anchor, '=J' . $anchor);
                $activeSheet->setCellValue('Q' . $anchor, '=SUM(K' . $anchor . ':O' . $anchor . ')');
            }


            $anchor++;
        }

        $styleArray = array
        (
            'borders' => array
            (
                'allborders' => array
                (
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array
                    (
                        'rgb' => '000000'
                    ),
                ),
            ),
            'alignment' => array
            (
                'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_STYLE_ALIGNMENT::VERTICAL_CENTER,
            )
        );
        $activeSheet->getStyle('A6:T' . $anchor)->applyFromArray($styleArray);
    }

    $objPHPExcel->getActiveSheet()->setTitle('Новый лист');

    $objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
    $objWriter->save($_SERVER['DOCUMENT_ROOT'] . '/reports/' . $snippet . '.xls');
    $objPHPExcel->disconnectWorksheets();
    unset($objPHPExcel);
}

$getDiscipline = mysql_query("SELECT * FROM disciplines") or die(mysql_error());
$disciplines = array();
while ($item = mysql_fetch_assoc($getDiscipline)) {
    $disciplines[] = $item;
}

$getDepartaments = mysql_query("SELECT id, name FROM departaments")
or die(mysql_error());
$departaments = array();
while ($item = mysql_fetch_assoc($getDepartaments)) {
    $departaments[] = $item;
}

$getGroup = mysql_query("SELECT id, name FROM groups")
or die(mysql_error());
$groups = array();
while ($item = mysql_fetch_assoc($getGroup)) {
    $groups[] = $item;
}

if (isset($_POST['GROUP_ID'])) {
    $groupInfo = array();
    $getCurrGroupInfo = mysql_query("SELECT * FROM groups WHERE id = '" . $_POST['GROUP_ID'] . "'")
    or die(mysql_error());
    $groupInfo = mysql_fetch_assoc($getCurrGroupInfo);
    echo json_encode($groupInfo);
}

if ($_POST['CREATE_ORDER'] == 1) {
    $filename = 'teacher_report_' . date('Y_m_d_H_i_s');
    createTeacherReport(
        $_SERVER['DOCUMENT_ROOT'] . '/excel_template/teacher.xls',
        array(
            'REPORT_TITLE' => 'Расчет педагогической нагрузки на 2013-2014 учебный год',
            'DEPARTMENT' => 'Информационных систем и технологий'
        ),
        $_POST['DISCIPLINES'],
        $filename
    );
    echo '/reports/' . $filename . '.xls';
}
